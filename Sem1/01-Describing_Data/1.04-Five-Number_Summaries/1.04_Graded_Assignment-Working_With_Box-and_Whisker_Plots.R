dffivenum <- function (data, index) {
  text(x=fivenum(data[,index]), y=0.5+index, labels=fivenum(data[,index]))
  text(x=median(data[,index])+0.1, y=index, labels=paste("Mean:", round(mean(data[,index]), digits=3), sep=" "))
}

labboxplot <- function (data, title, lab) {
  boxplot(data, horizontal=TRUE, xlab=lab, main=title, staplewex=1)
  text(x=fivenum(data), y=1.25, labels=fivenum(data))
  text(x=median(data), y=1.5, labels=paste("Mean:", round(mean(data), digits=3), sep=" "))
}

# Question 1: The data below shows the number of raisins in each of 14 boxes (1/2 oz.)
# of three different brands of raisins: 
sun.tyrant <- c(25,	28,	25,	28,	29,	24,	28,	24,	24,	28,	30,	24,	22,	27)
sun.acrid <- c(29,	31,	29,	26,	28,	22,	25,	29,	29,	27,	28,	23,	26,	29)
laminatekist <- c(25,	26,	26,	26,	26,	28,	27,	26,	25,	28,	24,	28,	27,	25)
raisin.brands <- data.frame(sun.tyrant, sun.acrid, laminatekist)

labboxplot(raisin.brands, "Raisin Brands", "Number of Raisins")
dffivenum(raisin.brands, 1)
dffivenum(raisin.brands, 2)
dffivenum(raisin.brands, 3)

# Question 2: Here is a set of data of college GPAs from a sample of students who 
# completed an AP course in statistics in their senior year at a fictional high school: 
college.gpa <- c(3.8, 3.8, 3.9, 3.9, 3.5, 3.5, 3.0, 3.5, 3.8, 4.0, 3.5, 2.5, 2.4, 1.9, 4.0, 3.8, 3.7, 3.5, 3.9, 3.2)

hist(college.gpa, breaks=seq(1.9,4,by=0.3), main="College GPA of High School Senior AP Statistics Students", xlim=c(1.9,4), xlab="GPA")
labboxplot(college.gpa, "College GPA of High School Senior AP Statistics Students", "GPA")

# Question 3: The Houses of Parliament in Ghyronmia (a hypothetical small country) have
# two political parties: the Purple Party and the Chartreuse Party. Twenty different 
# “tax cut” bills came up in the most recent Parliamentary session. If senators voted yes, 
# they were in support of a tax cut. The number of times (out of a possible 20) each senator 
# voted yes is shown below:
purple <- c(17, 18, 20, 15, 12, 15, 17, 19, 20, 17, 20, 18, 15, 9, 14, 16, 17, 14, 3, 15)
chartreuse <- c(11, 13, 12, 9, 17, 11, 10, 14, 7, 8, 8, 11, 12, 12, 13, 12, 14, 10, 10, 10)
political.parties <- data.frame(purple, chartreuse)

labboxplot(political.parties, "Political Party Votes", "Number of 'Yes' Votes")
dffivenum(political.parties, 1)
dffivenum(political.parties, 2)
